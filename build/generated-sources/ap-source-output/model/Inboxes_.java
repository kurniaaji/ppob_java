package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-06-11T08:21:59")
@StaticMetamodel(Inboxes.class)
public class Inboxes_ { 

    public static volatile SingularAttribute<Inboxes, String> totBills;
    public static volatile SingularAttribute<Inboxes, String> subs_name;
    public static volatile SingularAttribute<Inboxes, String> idPel;
    public static volatile SingularAttribute<Inboxes, String> bills;
    public static volatile SingularAttribute<Inboxes, Long> id;
    public static volatile SingularAttribute<Inboxes, String> switcher;

}