/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kurnia
 */
@Entity
@Table(name = "inboxes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inboxes.findAll", query = "SELECT i FROM Inboxes i")
    , @NamedQuery(name = "Inboxes.findById", query = "SELECT i FROM Inboxes i WHERE i.id = :id")
    , @NamedQuery(name = "Inboxes.findByIdPel", query = "SELECT i FROM Inboxes i WHERE i.idPel = :idPel")})
public class Inboxes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "id_pel")
    private String idPel;
     @Column(name = "bills")
    private String bills;
      @Column(name = "tot_bills")
    private String totBills;
      @Column(name = "switcher")
    private String switcher;
       @Column(name = "subs_name")
    private String subs_name;
      
    public Inboxes() {
    }

    public Inboxes(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdPel() {
        return idPel;
    }

    public void setIdPel(String idPel) {
        this.idPel = idPel;
    }
    

    public String getBill() {
        return bills;
    }

    public void setBill(String bills) {
        this.bills = bills;
    }
    public String getTotBill(String totBills) {
        return totBills;
    }

    public void setTotBill(String totBills) {
        this.totBills = totBills;
    }
    public String getSwicth() {
        return switcher;
    }

    public void setSwicth(String switcher) {
        this.switcher = switcher;
    }
    public String getSubsName(String subs_name) {
        return subs_name;
    }

    public void setSubsName(String subs_name) {
        this.subs_name = subs_name;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inboxes)) {
            return false;
        }
        Inboxes other = (Inboxes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Inboxes[ id=" + id + " ]";
    }
    
}
