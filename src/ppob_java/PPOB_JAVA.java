/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppob_java;
//import channel.SymphoniChannel;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.channel.NACChannel;
import org.jpos.iso.packager.GenericPackager;
/**
 *
 * @author kurnia
 */
public class PPOB_JAVA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
//         String iso48 ="SWID003538710745406101                                JOKO NOVANDA                                  R1M000000900000000000201809                000000218632000000000000000000000000000000000000000000000000000000000000000000000000000000000";
//    
//       System.out.println("ISI BIT48 :: "+iso48);
//        PostpaidProcessing post = new PostpaidProcessing();
//        String[] bit48 = post.parseBit48Inquiry(iso48, "0000");
//        System.out.println("==========================");
//        post.insertToDb(bit48[1],bit48[2],bit48[3],bit48[4],bit48[5]);
    
     System.out.println(String.format("%18s", "0000002426690259").replace(" ", "0"));
      Socket client = null;
        try {
            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            
            isoMsg.setMTI("2100");
            isoMsg.set(2, "99508");
//            isoMsg.set(2, "00339");
            isoMsg.set(11, String.format("%12s", "0123").replace(' ', '0'));
            isoMsg.set(12, tgl);
            isoMsg.set(26, "6021");
            isoMsg.set(32, "4510017");
            isoMsg.set(33, "4510206");
            isoMsg.set(41, "00000000000");
            
            
            String bit48 = "0000000"+String.format("%18s", "0000002426690259").replace(" ", "0")+ "01";
            
            String bit63 = "pids13100175                    ardidy                        jgfsefu                                           234978648         3275HFUFYA                        41253";
//            ;
//note: bypass dulu (testing tahap 1 tanpa merubah SA Symphoni)            
            isoMsg.set(63, bit63);
            
            byte[] datax = isoMsg.pack();
            String networkRequest = new String(datax);
            client = new Socket("127.0.0.1", 2301);
            client.setSoTimeout(6000);
            String trailer = new String(new char[]{10});

            networkRequest = "210040300041808100020599508000000264202201906101647266012074510017074567898000000000000004802700000000000002426690250  01174plg12110004                     Myelectricreload              Margahayu Jaya blok A no 165, Jln cemara no.6     1234567           3275BEKASI                        0000017113";
            int msgLen = networkRequest.length(); System.out.println("LEN : "+msgLen);
            networkRequest = networkRequest+trailer;
        
            
            System.out.println("REQUEST :: "+networkRequest);
            
            PrintWriter outgoing = new PrintWriter(client.getOutputStream());
            InputStreamReader incoming = new InputStreamReader(client.getInputStream());

            outgoing.print(networkRequest);
            outgoing.flush();

            int data;
            StringBuffer sb = new StringBuffer();

            while ((data = incoming.read()) != -1) {
                if (data == -1 || data == 255 || data == 10 || data == 65533) {
                    break;
                }

                sb.append((char) data);
            }
            
            System.out.println("RESPONSE :: "+sb.toString());

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
     
    }
    public static String[] getUserdata() {
        String[] userData = new String [10];
        userData[0] = "pids16060001";
        userData[1] = "egateza";
        userData[2] = "Kp. Kadubuluh Desa Sukasari Kecamatan Kaduhejo Pandeglang Banten";
        userData[3] = "08976588098";
        userData[4] = "Ega";
        userData[5] = "PANDEGLANG";
        userData[6] = "36.01";
        userData[7] = "42252";


        if (userData[1].length() > 30) {
            userData[1] = userData[1].substring(0, 30);
        }
        if (userData[2].length() > 50) {
            userData[2] = userData[2].substring(0, 50);
        }
        if (userData[3].length() > 18) {
            userData[3] = userData[3].substring(0, 18);
        }

        if (userData[5].length() > 30) {
            userData[5] = userData[5].substring(0, 10);
        }
        
        return userData;
    }
    
}
    

