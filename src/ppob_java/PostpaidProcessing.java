/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppob_java;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import model.Inboxes;


/**
 *
 * @author egateza
 */
public class PostpaidProcessing {
    public static String[] parseBit48Inquiry(String msg, String rc) {
        
        String[] hasil = new String[50];
        int[] seq1 = {7,12,1,2,32,25,5,15,4,9,9,
                     6,8,8,12,11,10,12,8,8,8,8,8,8                     
                    };

        int[] seq2 = {7,12,1,2,32,25,5,15,4,9,9,
                     6,8,8,12,11,10,12,8,8,8,8,8,8,
                     6,8,8,12,11,10,12,8,8,8,8,8,8
                    };

        int[] seq3 = {7,12,1,2,32,25,5,15,4,9,9,
                     6,8,8,12,11,10,12,8,8,8,8,8,8,
                     6,8,8,12,11,10,12,8,8,8,8,8,8,
                     6,8,8,12,11,10,12,8,8,8,8,8,8
                    };

        int[] seq4 = {7,12,1,2,32,25,5,15,4,9,9,
                     6,8,8,12,11,10,12,8,8,8,8,8,8,
                     6,8,8,12,11,10,12,8,8,8,8,8,8,
                     6,8,8,12,11,10,12,8,8,8,8,8,8,
                     6,8,8,12,11,10,12,8,8,8,8,8,8
                    };

        int[] seq = null;
           
        if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("1"))
            seq = seq1;
        else if(String.valueOf(msg.charAt(19)).equalsIgnoreCase("2"))
            seq = seq2;
        else if(String.valueOf(msg.charAt(19)).equalsIgnoreCase("3"))
            seq = seq3;
        else if(String.valueOf(msg.charAt(19)).equalsIgnoreCase("4"))
            seq = seq4;

        
        String[] title = {
            "switcher id",
            "subscriber id",
            "bill status",
            "total outstanding bill",
            "switcher refnum",
            "subscriber name",
            "service unit",
            "service unit phone",
            "subscriber segmentation",
            "power consuming category",
            "total admin charges",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;

            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            
        }
        
        return hasil;
        
        
    }
    
    public void insertToDb(String Idpel,String Bills,String totBills,String Swicth,String Subs_name ) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("PPOB_JAVAPU", System.getProperties());
        EntityManager em = factory.createEntityManager();
        try {
            

            Inboxes inbox = new Inboxes();
            em.getTransaction().begin();
            
            inbox.setIdPel(Idpel);
            inbox.setBill(Bills);
            inbox.setTotBill(totBills);
            inbox.setSwicth(Swicth);
            inbox.setSubsName(Subs_name);
            em.persist(inbox);
            em.getTransaction().commit();
            em.close();
            factory.close();
        } catch (Exception e) {
            System.err.println("gagal menyimpan");
            e.printStackTrace();
//            System.err.println(e.getMessage());
//            System.err.println(e);
        } finally {
            System.err.println("finally");

            try {
                em.close();
            } catch (Exception e) {
                System.err.println("EntityManager already closed");
            }
            try {
                factory.close();
            } catch (Exception e) {
                System.err.println("EntityManagerFactory already closed");
            }
        }
        
    }
}
